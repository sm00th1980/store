//
//  UITableViewCell+Good.m
//  store
//
//  Created by Деяров Руслан on 24.10.13.
//  Copyright (c) 2013 store. All rights reserved.
//


#import "UITableViewCell+Good.h"
#import <objc/runtime.h>

@implementation UITableViewCell (Good)
static char UIB_GOOD_KEY;

- (void)setGood:(Good *)good
{
    objc_setAssociatedObject(self, &UIB_GOOD_KEY, good, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (Good *)good
{
    return (Good *)objc_getAssociatedObject(self, &UIB_GOOD_KEY);
}
@end