//
//  UITableViewCell+Good.h
//  store
//
//  Created by Деяров Руслан on 24.10.13.
//  Copyright (c) 2013 store. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Good.h"

@interface UITableViewCell (Good)
@property (nonatomic, weak) Good *good;
@end
