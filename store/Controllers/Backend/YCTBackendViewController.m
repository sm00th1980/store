//
//  YCTBackendViewController.m
//  store
//
//  Created by Деяров Руслан on 22.10.13.
//  Copyright (c) 2013 store. All rights reserved.
//


#import "YCTBackendViewController.h"
#import "Good.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "YCTBackendGoodViewController.h"
#import "UITableViewCell+Good.h"

@interface YCTBackendViewController ()
@property (strong, nonatomic) NSArray *goods;
@end

@implementation YCTBackendViewController

-(IBAction)deleteAllGoods:(id)sender
{
    [UIAlertView alertViewWithTitle:NSLocalizedString(@"Delete all goods", nil)
                            message:NSLocalizedString(@"Are you sure to delete all goods?", nil)
                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                  otherButtonTitles: @[NSLocalizedString(@"Delete all goods", nil)]
                          onDismiss:^(int buttonIndex){
                              [Good removeAll:^(){
                                  [self prepareForShow];
                              }];
                          }
                           onCancel:^(){}];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self prepareForShow];
}

-(void)prepareForShow
{
    [Good allByNameASC:^(id goods){
        _goods = goods;
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_goods count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"backend cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    //customize cell
    Good *good = (Good *)_goods[indexPath.row];
    cell.textLabel.text = good.name;
    cell.textLabel.textColor = [UIColor blueColor];
    
    cell.detailTextLabel.text = good.details;
    
    cell.good = good;
    
    return cell;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UITableViewCell *cell = (UITableViewCell *)sender;
    
    if ([[segue identifier] isEqualToString:@"update good"]) {
        YCTBackendGoodViewController *vc = [segue destinationViewController];
        [vc setGood:cell.good];
    }
}

@end
