//
//  YCTBackendGoodViewController.m
//  store
//
//  Created by Деяров Руслан on 22.10.13.
//  Copyright (c) 2013 store. All rights reserved.
//
#define DELAY 5.0f //задержка на создание товара

#import "YCTBackendGoodViewController.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "YCTBackendViewController.h"

@interface YCTBackendGoodViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *categoryField;
@property (weak, nonatomic) IBOutlet UITextField *priceField;
@property (weak, nonatomic) IBOutlet UITextField *countField;

@property (strong, nonatomic) Good *good;
@end

@implementation YCTBackendGoodViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self prepareForShow];
}

- (void)prepareForShow
{
    if (_good) {
        //update form
        self.title = NSLocalizedString(@"Update good", nil);
        
        _nameField.text = _good.name;
        _categoryField.text = _good.category;
        _priceField.text = [NSString stringWithFormat:@"%.2f", [_good.price floatValue]];
        _countField.text = [NSString stringWithFormat:@"%i", [_good.count integerValue]];
    } else {
        //new form
        self.title = NSLocalizedString(@"New good", nil);
        
        _countField.text = @"0";
    }
}

- (void)setGood:(Good *)good
{
    _good = good;
}

- (IBAction)saveGood:(id)sender
{
    //возвращаемся на шаг назад
    YCTBackendViewController* tvc = (YCTBackendViewController *)[[self.navigationController viewControllers] firstObject];
    [self.navigationController popViewControllerAnimated:YES];
    
    //выполняем создание/обновление товара с задержкой
    [NSObject performBlock:^(){
        [Good createByName:_nameField.text category:_categoryField.text price:[_priceField.text floatValue] count:[_countField.text integerValue] completion:^(BOOL success, NSArray *errors){
            if (success) {
                //удаляем старый продукт
                [_good remove];
                
                [tvc prepareForShow];
            } else {
                //при валидации позникли ошибки -> покажем их пользователю
                [UIAlertView alertViewWithTitle:NSLocalizedString(@"Save not finished", nil)
                                        message:[errors componentsJoinedByString:@", "]
                              cancelButtonTitle:nil
                              otherButtonTitles:@[NSLocalizedString(@"OK", nil)]
                                      onDismiss:^(int buttonIndex){}
                                       onCancel:^(){}];
            }
        }];
    } afterDelay:DELAY];
    
    
    
}

@end
