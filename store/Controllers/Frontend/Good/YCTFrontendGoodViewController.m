//
//  YCTFrontendGoodViewController.m
//  store
//
//  Created by Деяров Руслан on 24.10.13.
//  Copyright (c) 2013 store. All rights reserved.
//

#define DELAY 3.0f //задержка на покупку товара

#import "YCTFrontendGoodViewController.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "YCTFrontendViewController.h"

@interface YCTFrontendGoodViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property (strong, nonatomic) Good *good;
@end

@implementation YCTFrontendGoodViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self prepareForShow];
}

- (void)prepareForShow
{
    //update form
    self.title = NSLocalizedString(@"Details", nil);
    
    _nameLabel.text = _good.name;
    _categoryLabel.text = _good.category;
    _priceLabel.text = [NSString stringWithFormat:@"%.2f", [_good.price floatValue]];
    _countLabel.text = [NSString stringWithFormat:@"%i", [_good.count integerValue]];
}

- (void)setGood:(Good *)good
{
    _good = good;
}

- (IBAction)buyGood:(id)sender
{
    //возвращаемся на шаг назад
    YCTFrontendViewController* tvc = (YCTFrontendViewController *)[[self.navigationController viewControllers] firstObject];
    [self.navigationController popViewControllerAnimated:YES];
    
    //выполняем покупку товара с задержкой
    [NSObject performBlock:^(){
        [_good buy];
        [tvc prepareForShow];
    } afterDelay:DELAY];
}

@end
