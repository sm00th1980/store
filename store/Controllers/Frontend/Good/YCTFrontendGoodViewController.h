//
//  YCTFrontendGoodViewController.h
//  store
//
//  Created by Деяров Руслан on 24.10.13.
//  Copyright (c) 2013 store. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Good.h"

@interface YCTFrontendGoodViewController : UIViewController
- (void)setGood:(Good *)good;
@end
