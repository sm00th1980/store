//
//  YCTFrontendViewController.m
//  store
//
//  Created by Деяров Руслан on 22.10.13.
//  Copyright (c) 2013 store. All rights reserved.
//

#import "YCTFrontendViewController.h"
#import "Good.h"
#import "UITableViewCell+Good.h"
#import "YCTFrontendGoodViewController.h"

@interface YCTFrontendViewController ()
@property (strong, nonatomic) NSArray *goods;
@end

@implementation YCTFrontendViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self prepareForShow];
}

-(void)prepareForShow
{
    [Good availableForBuy:^(NSArray *goods){
        _goods = goods;
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_goods count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"frontend cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    Good *good = (Good *)_goods[indexPath.row];
    
    cell.textLabel.text = good.name;
    cell.textLabel.textColor = [UIColor blueColor];
    
    cell.detailTextLabel.text = good.details;
    
    cell.good = good;
    
    return cell;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UITableViewCell *cell = (UITableViewCell *)sender;
    
    if ([[segue identifier] isEqualToString:@"good details"]) {
        YCTFrontendGoodViewController *vc = [segue destinationViewController];
        [vc setGood:cell.good];
    }
}
@end
