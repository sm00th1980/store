//
//  Good.m
//  store
//
//  Created by Деяров Руслан on 22.10.13.
//  Copyright (c) 2013 store. All rights reserved.
//

#import "Good.h"


@implementation Good

@synthesize errors = _errors;

@dynamic category;
@dynamic count;
@dynamic name;
@dynamic price;

#pragma mark - public interface

+ (void)createByName:(NSString *)name category:(NSString *)category price:(float)price count:(int)count completion:(void_block_bool_arr)comletion
{
    Good *new_good = [Good newObject];
    new_good.name = name;
    new_good.category = category;
    new_good.price = [NSNumber numberWithFloat:price];
    new_good.count = [NSNumber numberWithInteger:count];
    
    if ([new_good isValid]) {
        [new_good save]; //сохраняем объект
        
        //при валидации ошибок не было
        comletion(YES, @[]);
    } else {
        //при валидации возникли ошибки
        comletion(NO, [new_good errors]);
    }
}

+ (void)availableForBuy:(void_block_arr)completion
{
    NSPredicate *search = [NSPredicate predicateWithFormat:@"count > %@", @0];
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    [self where:search sort:@[nameDescriptor] finished:^(id goods){
        completion(goods);
    }];
}

+ (void)allByNameASC:(void_block_arr)completion
{
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    [self all:@[nameDescriptor] finished:^(id goods){
        completion(goods);
    }];
}

- (NSString *)details
{
    return [NSString stringWithFormat:@"Category: %@, Price: %.2f, Count: %d", self.category, [self.price floatValue], [self.count integerValue]];
}

+ (void)removeAll:(void_block_void)completion
{
    [self all:^(id goods){
        [self destroyObjects:(NSArray *)goods];
        completion();
    }];
}

- (void)remove
{
    [Good destroyObject:self];
}

- (void)buy
{
    NSInteger prev_count = [self.count integerValue];
    NSInteger new_count = 0;
    
    if (prev_count > 0) {
        new_count = prev_count - 1;
    }
    
    self.count = [NSNumber numberWithInteger:new_count];
    [self save];
}

#pragma mark - private interface

- (BOOL)isValid
{
    //валидация
    //1)поле name должно быть не пусто - обязательный параметр
    //2)поле category - optional
    //3)поле price должно быть больше 0
    //4)поле count должно быть >= 0
    if (_errors) {
        [_errors removeAllObjects];
    } else {
        _errors = [NSMutableArray array];
    }
    
    //проверяем name
    if (!self.name || [self.name isEqualToString:@""]) {
        [_errors addObject:NSLocalizedString(@"Name shouldn't be blank", nil)];
    }
    
    //проверяем category
    if (!self.category || [self.category isEqualToString:@""]) {
        self.category = NSLocalizedString(@"Nope", nil);
    }
    
    //проверяем price
    if (!self.price || [self.price floatValue] <= 0.0f) {
        [_errors addObject:NSLocalizedString(@"Price should be greater 0.0", nil)];
    }
    
    //проверяем count
    if (!self.count || [self.count integerValue] < 0.0f) {
        [_errors addObject:NSLocalizedString(@"Count should be greater or equal 0", nil)];
    }
    
    if ([_errors count] > 0) {
        return NO;
    }
    
    return YES;
}

@end
