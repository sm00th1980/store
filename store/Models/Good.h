//
//  Good.h
//  store
//
//  Created by Деяров Руслан on 22.10.13.
//  Copyright (c) 2013 store. All rights reserved.
//

typedef void (^void_block_bool_arr)(BOOL success, NSArray *errors);
typedef void (^void_block_arr)(NSArray *goods);
typedef void (^void_block_void)(void);

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DCModel.h"

@interface Good : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSNumber * count;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * price;

@property (nonatomic, strong) NSMutableArray * errors;

+ (void)createByName:(NSString *)name category:(NSString *)category price:(float)price count:(int)count completion:(void_block_bool_arr)comletion;
- (NSString *)details;

+ (void)availableForBuy:(void_block_arr)completion;
+ (void)allByNameASC:(void_block_arr)completion;

- (void)remove;
+ (void)removeAll:(void_block_void)completion;

- (void)buy;
@end
