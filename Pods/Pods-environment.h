
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// BlocksKit
#define COCOAPODS_POD_AVAILABLE_BlocksKit
#define COCOAPODS_VERSION_MAJOR_BlocksKit 1
#define COCOAPODS_VERSION_MINOR_BlocksKit 8
#define COCOAPODS_VERSION_PATCH_BlocksKit 3

// DCModel
#define COCOAPODS_POD_AVAILABLE_DCModel
#define COCOAPODS_VERSION_MAJOR_DCModel 0
#define COCOAPODS_VERSION_MINOR_DCModel 0
#define COCOAPODS_VERSION_PATCH_DCModel 2

// UIKitCategoryAdditions
#define COCOAPODS_POD_AVAILABLE_UIKitCategoryAdditions
#define COCOAPODS_VERSION_MAJOR_UIKitCategoryAdditions 0
#define COCOAPODS_VERSION_MINOR_UIKitCategoryAdditions 0
#define COCOAPODS_VERSION_PATCH_UIKitCategoryAdditions 1

// libffi
#define COCOAPODS_POD_AVAILABLE_libffi
#define COCOAPODS_VERSION_MAJOR_libffi 3
#define COCOAPODS_VERSION_MINOR_libffi 0
#define COCOAPODS_VERSION_PATCH_libffi 13

